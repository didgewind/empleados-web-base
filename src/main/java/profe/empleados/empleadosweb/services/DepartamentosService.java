package profe.empleados.empleadosweb.services;

import profe.empleados.model.Departamento;

public interface DepartamentosService {

	Departamento[] getAllDepartamentos();
}
